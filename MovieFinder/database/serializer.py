from .models import *
from rest_framework import serializers

class SerializerSeller(serializers.ModelSerializer):
    class Meta:
        model = MovieFinder
        fields = ['nama','umur','genre','tahun','asal','listfilm','favorite']