from django.shortcuts import render
from .models import *
from .serializer import *
from rest_framework import viewsets

def Home(request):
    movie = MovieFinder.objects.all()
    return render(request, 'index.html', {'movie':movie})


class ViewsetData(viewsets.ModelViewSet):
    queryset = MovieFinder.objects.all()
    serializer_class = SerializerSeller
