from django.db import models

class MovieFinder(models.Model):
    nama = models.TextField(max_length=30)
    umur = models.IntegerField()
    genre = models.TextField(max_length=100)
    tahun = models.IntegerField()
    asal = models.TextField(max_length=100)
    listfilm = models.TextField(max_length=100)
    favorite = models.TextField(max_length=100)

    def __str__(self):
        return self.nama
